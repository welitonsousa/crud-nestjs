import { Repository, EntityRepository } from "typeorm";
import User from "./user.entity";
import { CreateUserDto } from "./dtos/create-user.dto";
import { UserRole } from "./user-roles.enum";
import * as crypto from 'crypto';
import * as bcrypt from 'bcrypt';
import { ConflictException, InternalServerErrorException } from "@nestjs/common";
import { CredentialsDto } from "src/auth/DTO/credentials.dto";

@EntityRepository(User)
export class UserRepository extends Repository<User>{
  async createUser(createUserDto: CreateUserDto, roles: UserRole): Promise<User> {
    const { email, nome, password } = createUserDto;
    const user = this.create();

    user.email = email;
    user.nome = nome;
    user.password = password;
    user.role = roles;
    user.status = true;
    user.confirmationToken = crypto.randomBytes(32).toString('hex');
    user.salt = await bcrypt.genSalt();
    user.password = await bcrypt.hash(password, user.salt);
    try {
      await user.save();
      delete user.password;
      delete user.salt;
      return user;
    } catch (error) {
      if (error.code.toString() === '23505') {
        throw new ConflictException('Endereço de email já está em uso');
      } else {
        throw new InternalServerErrorException(
          'Erro ao salvar o usuário no banco de dados',
        );
      }
    }

  } async getAllAdminUser(): Promise<User[]> {
    const users = await this.find()
    return users;
  }
  async checkCredentials(credentialsDto: CredentialsDto): Promise<User> {
    const { email, password } = credentialsDto;
    const user = await this.findOne({ email, status: true })
    if (user && user.checkPassword(password)) {
      return user;
    } else {
      return null;
    }
  }
}