import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { UserRepository } from './users.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserDto } from './dtos/create-user.dto';
import { UserRole } from './user-roles.enum';
import User from './user.entity';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(UserRepository)
        private userRepository: UserRepository
    ){}
    async createAdminUser (createUserDto: CreateUserDto): Promise<User>{

        if(createUserDto.password != createUserDto.passwordConfirmation)
        {
            throw new UnprocessableEntityException('As senhas não conferem')
        }else{
            return this.userRepository.createUser(createUserDto, UserRole.ADMIN)
        }
    }
    async getAllAdminUsers(): Promise<User[]>{
        const users = await this.userRepository.getAllAdminUser()
        return users;
    }
}
