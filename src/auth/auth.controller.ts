import { Controller, Post, ValidationPipe, Body, Get, UseGuards, } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDto } from 'src/users/dtos/create-user.dto';
import { CredentialsDto } from './DTO/credentials.dto';
import { AuthGuard } from '@nestjs/passport';
import User from 'src/users/user.entity';
import { GetUser } from 'src/users/user.decorator';

@Controller('auth')
export class AuthController {
    constructor(
        private authService: AuthService
    ) { }
    @Post('/signup')
    async signUp(@Body(ValidationPipe) createUserDto: CreateUserDto,): Promise<{ message: string }> {
        await this.authService.signUp(createUserDto)
        return { message: 'Cadastro realizado com sucesso' }
    }
    @Post('/signin')
    async signIn(@Body() credentialsDto: CredentialsDto): Promise<{ token: string }> {
        return await this.authService.signIn(credentialsDto);
    }

    @Get('/me')
    @UseGuards(AuthGuard())
    getMe(
        @GetUser() user:User): User {
        return user;
    }
}

